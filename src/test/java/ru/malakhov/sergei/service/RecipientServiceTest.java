package ru.malakhov.sergei.service;

import com.sun.istack.NotNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.exception.EmptyIdException;
import ru.malakhov.sergei.exception.EmptyRecipientException;
import ru.malakhov.sergei.exception.ExistRecipientException;

@SpringBootTest
class RecipientServiceTest {

    @NotNull
    @Autowired
    private RecipientService recipientService;

    private final Recipient recipient1 = new Recipient("1","1","1","1@1",1);
    private final Recipient recipient2 = new Recipient("2","2","2","2@2",2);

    @BeforeEach
    public void initData() throws ExistRecipientException, EmptyRecipientException {
        recipientService.deleteAll();
        recipientService.save(recipient1);
    }

    @AfterEach
    public void cleanData() {
        recipientService.deleteAll();
    }

    @Test
    void findAll() throws ExistRecipientException, EmptyRecipientException {
        Assertions.assertEquals(1,recipientService.findAll().size());
        recipientService.save(recipient2);
        Assertions.assertEquals(2,recipientService.findAll().size());
    }

    @Test
    void findAllEmail() throws ExistRecipientException, EmptyRecipientException {
        Assertions.assertEquals(1,recipientService.findAllEmail().size());
        recipientService.save(recipient2);
        Assertions.assertEquals(2,recipientService.findAllEmail().size());
    }

    @Test
    void save() {
        Assertions.assertEquals(1,recipientService.findAllEmail().size());
        Assertions.assertThrows(ExistRecipientException.class, ()->recipientService.save(recipient1));
    }

    @Test
    void findById() throws EmptyIdException {
        Assertions.assertThrows(EmptyIdException.class,()->recipientService.findById(""));
        Recipient findRecipient = recipientService.findById(recipient1.getId());
        Assertions.assertEquals(recipient1.getId(),findRecipient.getId());
    }

    @Test
    void deleteById() throws EmptyIdException {
          Assertions.assertEquals(1,recipientService.findAll().size());
          recipientService.deleteById(recipient1.getId());
          Assertions.assertEquals(null,recipientService.findById(recipient1.getId()));
    }

}