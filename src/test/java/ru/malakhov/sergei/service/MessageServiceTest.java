package ru.malakhov.sergei.service;

import com.sun.istack.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.test.context.ContextConfiguration;
import ru.malakhov.sergei.AutoMessageSenderApplication;
import ru.malakhov.sergei.constant.MessageConst;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.exception.EmptyRecipientException;
import ru.malakhov.sergei.exception.ExistRecipientException;


@SpringBootTest
@ContextConfiguration(classes = AutoMessageSenderApplication.class)
class MessageServiceTest {

    @NotNull
    @Autowired
    private RecipientService recipientService;

    @BeforeEach
    public void initData() throws ExistRecipientException, EmptyRecipientException {
        recipientService.deleteAll();
    }
    final Recipient recipient1 = new Recipient("1","1","1","1@1",1);

    @NotNull final SimpleMailMessage teaMessage = new SimpleMailMessage();

    @Test
    void sentMessage() {
        teaMessage.setFrom(MessageConst.EMAIL_FROM);
        teaMessage.setTo(recipient1.getEmail());
        teaMessage.setSubject("MessageConst.MESSAGE_SUBJECT");
        teaMessage.setText("MessageConst.MESSAGE_TEXT");
        Assertions.assertEquals(MessageConst.EMAIL_FROM,teaMessage.getFrom());
        Assertions.assertEquals("MessageConst.MESSAGE_SUBJECT",teaMessage.getSubject());
        Assertions.assertEquals("MessageConst.MESSAGE_TEXT",teaMessage.getText());
    }

}