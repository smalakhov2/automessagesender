package ru.malakhov.sergei.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.exception.EmptyIdException;
import ru.malakhov.sergei.exception.EmptyRecipientException;
import ru.malakhov.sergei.exception.ExistRecipientException;

import java.util.List;

public interface IRecipientService {

    @NotNull
    @Transactional(readOnly = true)
    List<Recipient> findAll();

    @NotNull
    @Transactional(readOnly = true)
    List<String> findAllEmail();

    @Nullable
    @Transactional(rollbackForClassName = {"Exception"})
    Recipient save(@Nullable final Recipient recipient) throws EmptyRecipientException, ExistRecipientException;

    @Nullable
    @Transactional(readOnly = true)
    Recipient findById(@Nullable final String id) throws EmptyIdException;

    @Transactional(rollbackForClassName = {"Exception"})
    void deleteById(@Nullable final String id) throws EmptyIdException;

}