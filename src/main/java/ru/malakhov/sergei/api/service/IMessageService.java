package ru.malakhov.sergei.api.service;

import ru.malakhov.sergei.entity.Message;
import ru.malakhov.sergei.exception.AbstractException;

public interface IMessageService {

    void sentMessage();

    void saveMessage(Message message);

}