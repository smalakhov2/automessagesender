package ru.malakhov.sergei.api.service;

import ru.malakhov.sergei.entity.Statistic;
import ru.malakhov.sergei.exception.AbstractException;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface IStatisticService {
    Map<String,List<List<Object>>> getChartData();
    void save(Statistic statistic) throws AbstractException;
    void saveStatistic(int counter, int messageCount, LocalDate date);
}
