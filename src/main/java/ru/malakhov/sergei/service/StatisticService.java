package ru.malakhov.sergei.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.sergei.constant.StatisticConst;
import ru.malakhov.sergei.exception.AbstractException;
import ru.malakhov.sergei.api.service.IStatisticService;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.entity.Statistic;
import ru.malakhov.sergei.repository.RecipientRepository;
import ru.malakhov.sergei.repository.StatisticIoRepository;
import ru.malakhov.sergei.repository.StatisticRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatisticService implements IStatisticService {

    @Autowired
    RecipientRepository recipientRepository;

    @Autowired
    StatisticRepository statisticRepository;
    @Autowired
    StatisticIoRepository statisticIoRepository;

    @Override
    public Map<String,List<List<Object>>> getChartData() {
        Map<String,List<List<Object>>> mapChart = new HashMap<>();
        mapChart.put(StatisticConst.DIAGRAM,getDiagramChart());
        mapChart.put(StatisticConst.BAR, getBarChart());
        mapChart.put(StatisticConst.COUNT,getCountChart());
        return mapChart;
    }

    @Override
    public void save(Statistic statistic) throws AbstractException {
        if (statistic == null) throw new AbstractException("Объект статистики не найден");
        statisticRepository.save(statistic);
    }

    @Override
    public void saveStatistic(int counter, int messageCount, LocalDate date) {
        Statistic statistic = new Statistic();
        statistic.setCounter(counter);
        statistic.setMessageCount(messageCount);
        statistic.setDate(date);
        statisticRepository.save(statistic);
    }

    private List<List<Object>> getDiagramChart() {
        List<Recipient> recipients = recipientRepository.findAll();
        List<List<Object>> objects = new ArrayList<>();
        int minRange = 0;
        int middleRange = 0 ;
        int maxRange = 0;
        for (Recipient recipient: recipients) {
            if (recipient.getAge() < StatisticConst.MIN_RANGE + 1) {
                minRange ++;
            } else if (recipient.getAge() < StatisticConst.MIDDLE_RANGE + 1) {
                middleRange++;
            } else {
                maxRange++;
            }
        }
        objects.add(List.of(getMinDiapason(), minRange));
        objects.add(List.of(getMiddleDiapason(), middleRange));
        objects.add(List.of(getMaxDiapason(), maxRange));
        return objects;
    }

    private String getMinDiapason() {
        return "0 - " + StatisticConst.MIN_RANGE;
    }

    private String getMiddleDiapason() {
        return StatisticConst.MIN_RANGE + 1 + " - " + StatisticConst.MIDDLE_RANGE;
    }

    private String getMaxDiapason() {
        return StatisticConst.MIDDLE_RANGE + 1 + " > ";
    }

    private List<List<Object>> getBarChart() {
        List<Object[]> statistics = statisticIoRepository.getCount();
        List<List<Object>> objects = new ArrayList<>();
        for (Object[] statistic: statistics) {
            objects.add(List.of(statistic[0], statistic[1]));
        }
        return objects;
    }

    private List<List<Object>> getCountChart() {
        List<Object[]> statistics = statisticIoRepository.getCountMonth();
        List<List<Object>> objects = new ArrayList<>();
        for (Object[] statistic: statistics) {
            objects.add(List.of(String.valueOf(statistic[0]), statistic[1]));
        }
        return objects;
    }
}
