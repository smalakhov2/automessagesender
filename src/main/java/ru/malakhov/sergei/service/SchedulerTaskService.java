package ru.malakhov.sergei.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.malakhov.sergei.api.service.IMessageService;

@Service
public class SchedulerTaskService {

    @NotNull
    private final IMessageService messageService;

    @Autowired
    public SchedulerTaskService(@NotNull final IMessageService messageService) {
        this.messageService = messageService;
    }

    public void sentMessage()  {
        messageService.sentMessage();
    }

}