package ru.malakhov.sergei.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.sergei.exception.EmptyIdException;
import ru.malakhov.sergei.exception.EmptyRecipientException;
import ru.malakhov.sergei.exception.ExistRecipientException;
import ru.malakhov.sergei.api.service.IRecipientService;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.repository.RecipientRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecipientService implements IRecipientService {

    @NotNull
    private final RecipientRepository recipientRepository;

    public RecipientService(@NotNull final RecipientRepository recipientRepository) {
        this.recipientRepository = recipientRepository;
    }

    @NotNull
    @Transactional(readOnly = true)
    public List<Recipient> findAll() {
        return recipientRepository.findAll();
    }

    @NotNull
    @Transactional(readOnly = true)
    public List<String> findAllEmail() {
        @NotNull final List<String> emails = new ArrayList<>();
        recipientRepository.findALLEmail().forEach(emails::add);
        return emails;
    }

    @Nullable
    @Transactional(rollbackForClassName = {"Exception"})
    public Recipient save(@Nullable final Recipient recipient) throws EmptyRecipientException, ExistRecipientException {
        if (recipient == null || recipient.getEmail().isEmpty()) throw new EmptyRecipientException();
        if(recipientRepository.checkExist(recipient.getEmail()) > 0) throw new ExistRecipientException();
        return recipientRepository.save(recipient);
    }

    @Nullable
    @Transactional(readOnly = true)
    public Recipient findById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return recipientRepository.findById(id).orElse(null);
    }

    @Transactional(rollbackForClassName = {"Exception"})
    public void deleteById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        recipientRepository.deleteById(id);
    }

    @Nullable
    @Transactional(rollbackForClassName = {"Exception"})
    public void deleteAll() {
        recipientRepository.deleteAll();
    }

}
