package ru.malakhov.sergei.service;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.malakhov.sergei.constant.MessageConst;
import ru.malakhov.sergei.exception.AbstractException;
import ru.malakhov.sergei.api.service.IMessageService;
import ru.malakhov.sergei.api.service.IRecipientService;
import ru.malakhov.sergei.api.service.IStatisticService;
import ru.malakhov.sergei.entity.Message;
import ru.malakhov.sergei.repository.MessageRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class MessageService implements IMessageService {

    @NotNull
    private final IRecipientService recipientService;

    @NotNull
    private final IStatisticService statisticService;

    @NotNull
    private final JavaMailSender javaMailSender;

    @Autowired
    MessageRepository messageRepository;

    public MessageService(IRecipientService recipientService, IStatisticService statisticService, JavaMailSender javaMailSender) {
        this.recipientService = recipientService;
        this.statisticService = statisticService;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sentMessage() {
        @NotNull final String[] emails = recipientService.findAllEmail().toArray(new String[] {});
        @NotNull final List<Message> messageList = messageRepository.findAll();
        int counterMessage = 0;
        int messageCount = 0 ;
        for (Message mes: messageList) {
            messageCount++;
            if (Objects.equals(mes.getSchedule(), LocalDate.now())) {
                sendMessage(mes, emails);
                mes.setDayTag(mes.getDayTag()-1);
                mes.setSendTag(mes.getSendTag()+1);
                counterMessage++;
                messageRepository.save(mes);
            }
        }
        statisticService.saveStatistic(counterMessage, messageCount,LocalDate.now());
    }

    private void sendMessage(@NotNull final Message messageObject, String[] emails) {
        @NotNull final SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(MessageConst.EMAIL_FROM);
        message.setTo(emails);
        message.setSubject(messageObject.getMessageSubject());
        message.setText(messageObject.getMessage());
        javaMailSender.send(message);
    }

    @Override
    public void saveMessage(Message message) {
        if (message != null) {
            messageRepository.save(message);
        }
    }
}