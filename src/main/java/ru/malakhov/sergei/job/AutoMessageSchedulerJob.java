package ru.malakhov.sergei.job;

import lombok.extern.log4j.Log4j2;
import org.quartz.DisallowConcurrentExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.sergei.configuration.properties.JobConfigProperties;
import ru.malakhov.sergei.service.SchedulerTaskService;

import java.util.List;

@Log4j2
@Component
@DisallowConcurrentExecution
public class AutoMessageSchedulerJob extends AbstractJob{

    @Autowired
    private JobConfigProperties.Job autoMessageSchedulerJobProperties;

    @Autowired
    private SchedulerTaskService taskService;


    @Override
    public String getName() {
        return autoMessageSchedulerJobProperties.getJobName();
    }

    @Override
    public void execute() {
        taskService.sentMessage();
    }

    @Override
    public List<JobConfigProperties.Trigger> getTriggers() {
        return autoMessageSchedulerJobProperties.getTriggers();
    }
}
