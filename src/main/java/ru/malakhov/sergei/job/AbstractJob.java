package ru.malakhov.sergei.job;

import lombok.extern.log4j.Log4j2;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import ru.malakhov.sergei.configuration.properties.JobConfigProperties;

import java.util.List;


@Log4j2
public abstract class AbstractJob implements Job {

    @Override
    public void execute(JobExecutionContext context) {
        String name = getName();
        try {
            execute();
        } catch (Exception e) {
            log.error(String.format("Error during %s Job executing: ", name), e);
        }
    }

    public abstract String getName();

    public abstract void execute();

    public abstract List<JobConfigProperties.Trigger> getTriggers();
}
