package ru.malakhov.sergei.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "app_message")
public class Message {

    @Id
    @NonNull
    private String id = UUID.randomUUID().toString();

    private String messageSubject;

    private String message;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate schedule;

    private int dayTag;

    private int sendTag;
}
