package ru.malakhov.sergei.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@Entity
public class StatisticIo {
    @Id
    @NonNull
    private String id = UUID.randomUUID().toString();

    private LocalDate date;

    private int counter;
}
