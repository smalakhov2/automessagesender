package ru.malakhov.sergei.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "app_stat")
public class Statistic {

    @Id
    @NonNull
    private String id = UUID.randomUUID().toString();
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    private int counter;

    private int messageCount;

    public Statistic() {
    }

}
