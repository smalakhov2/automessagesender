package ru.malakhov.sergei.entity;


import lombok.*;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;


@Getter
@Setter
@Entity
@AllArgsConstructor
@Table(name = "app_recipient")
public class Recipient {

    @Id
    @NonNull
    @Column(columnDefinition = "VARCHAR(255)")
    private String id = UUID.randomUUID().toString();

    @Nullable
    private String firstname,lastname;

    @NonNull
    @Column(unique = true, nullable = false)
    private String email;

    private int age;

    public Recipient() {
    }
}