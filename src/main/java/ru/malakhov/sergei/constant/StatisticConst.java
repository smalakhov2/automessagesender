package ru.malakhov.sergei.constant;

public interface StatisticConst {
    int MIN_RANGE = 18;
    int MIDDLE_RANGE = 36;
    int MAX_RANGE = 54;
    String DIAGRAM = "dgrm";
    String BAR = "br";
    String COUNT = "cn";
}
