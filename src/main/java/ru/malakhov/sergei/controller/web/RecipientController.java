package ru.malakhov.sergei.controller.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.sergei.api.service.IRecipientService;
import ru.malakhov.sergei.entity.Recipient;
import ru.malakhov.sergei.exception.EmptyIdException;

@Controller
@RequestMapping("/recipient")
public class RecipientController {

    @NotNull
    private final IRecipientService recipientService;

    @Autowired
    public RecipientController(@NotNull final IRecipientService subscriberService) {
        this.recipientService = subscriberService;
    }

    @GetMapping("/create")
    public ModelAndView createRecipient(){
        return new ModelAndView("recipient_edit", "recipient", new Recipient());
    }

    @PostMapping("/save/{id}")
    public ModelAndView saveRecipient(@ModelAttribute("recipient") @NotNull final Recipient recipient,
            @NotNull final BindingResult result
    ) {
        try {
           recipientService.save(recipient);
           return new ModelAndView("redirect:/recipients");
        } catch (Exception e) {
            return new ModelAndView("redirect:/recipients");
        }

    }

    @GetMapping("/edit/{id}")
    public ModelAndView editRecipient(@PathVariable("id") @Nullable final String id) throws EmptyIdException {
        @Nullable final Recipient recipient =  recipientService.findById(id);
        if (recipient == null) return new ModelAndView("redirect:/recipients");
        return new ModelAndView("recipient_edit", "recipient", recipient);
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteRecipient(@PathVariable @Nullable final String id) throws EmptyIdException {
        recipientService.deleteById(id);
        return new ModelAndView("redirect:/recipients");
    }

}