package ru.malakhov.sergei.controller.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.sergei.api.service.IMessageService;
import ru.malakhov.sergei.entity.Message;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {

    @NotNull
    private final IMessageService messageService;

    public ScheduleController(@NotNull final IMessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public ModelAndView viewScheduleForm(){
        return new ModelAndView("message", "message", new Message());
    }

    @PostMapping("/save")
    public ModelAndView setSchedule(@ModelAttribute("message") @NotNull final Message message) {
        messageService.saveMessage(message);
        return new ModelAndView("message", "message", new Message());
    }
}
