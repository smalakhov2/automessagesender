package ru.malakhov.sergei.controller.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.malakhov.sergei.api.service.IStatisticService;

@Controller
@RequestMapping("/statistic")
public class StatisticController {
    @NotNull
    private final IStatisticService statisticService;

    public StatisticController(@NotNull IStatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping
    public String getStatistic(Model model) {
        model.addAttribute("chartData", statisticService.getChartData());
        return "statistic";
    }

}
