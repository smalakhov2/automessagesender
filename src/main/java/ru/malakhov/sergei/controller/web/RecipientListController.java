package ru.malakhov.sergei.controller.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.sergei.api.service.IRecipientService;

@Controller
@RequestMapping("/recipients")
public class RecipientListController {

    @NotNull
    private final IRecipientService recipientService;

    @Autowired
    public RecipientListController(@NotNull final IRecipientService subscriberService) {
        this.recipientService = subscriberService;
    }

    @GetMapping
    public ModelAndView viewRecipientList(){
        return new ModelAndView("recipients", "recipients", recipientService.findAll());
    }

}