package ru.malakhov.sergei.repository;

import org.springframework.data.jpa.repository.Query;
import ru.malakhov.sergei.entity.Recipient;

public interface RecipientRepository extends AbstractRepository<Recipient> {

    @Query(value = "SELECT e.email FROM Recipient as e")
    Iterable<String> findALLEmail();

    @Query(value = "SELECT count(e.email) FROM Recipient as e where e.email =?1")
    int checkExist(String email);

}