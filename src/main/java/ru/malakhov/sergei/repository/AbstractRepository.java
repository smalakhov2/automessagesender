package ru.malakhov.sergei.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malakhov.sergei.entity.Recipient;

public interface AbstractRepository<E extends Recipient> extends JpaRepository<E, String> {
}