package ru.malakhov.sergei.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malakhov.sergei.entity.Message;

public interface MessageRepository extends JpaRepository<Message,String> {
}