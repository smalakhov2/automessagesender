package ru.malakhov.sergei.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.malakhov.sergei.entity.StatisticIo;

import java.util.List;

public interface StatisticIoRepository extends JpaRepository<StatisticIo,String> {
    @Query("SELECT e.date, SUM(e.counter) as counter FROM Statistic as e WHERE MONTH(e.date) = MONTH(NOW())" +
            "GROUP BY e.date")
    List<Object[]> getCount();

    @Query("SELECT MONTH(e.date), SUM(e.counter) as counter FROM Statistic as e GROUP BY e.date")
    List<Object[]> getCountMonth();
}
