package ru.malakhov.sergei.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malakhov.sergei.entity.Statistic;

public interface StatisticRepository extends JpaRepository<Statistic,String> {
}
