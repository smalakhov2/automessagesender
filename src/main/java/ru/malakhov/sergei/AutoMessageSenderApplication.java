package ru.malakhov.sergei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class AutoMessageSenderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoMessageSenderApplication.class,args);
    }

}