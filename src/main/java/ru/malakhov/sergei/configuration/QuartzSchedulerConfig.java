package ru.malakhov.sergei.configuration;

import lombok.extern.log4j.Log4j2;
import org.quartz.*;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import ru.malakhov.sergei.configuration.properties.JobConfigProperties;
import ru.malakhov.sergei.job.AbstractJob;

import javax.sql.DataSource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
@Configuration
@EnableScheduling
public class QuartzSchedulerConfig {
    private static final String SCHEDULER_NAME = "autoMessageScheduler";

    @Value("${scheduler.enabled}")
    private String isSchedulerEnabled;

    private List<AbstractJob> jobs;

    public QuartzSchedulerConfig(@Lazy List<AbstractJob> jobs) {
        this.jobs = jobs;
    }

    @Bean("quartzDS")
    @ConfigurationProperties("org.quartz.datasource")
    public DataSource quartzDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public Scheduler scheduler(SpringBeanJobFactory springBeanJobFactory, SchedulerFactoryBean schedulerFactory) throws SchedulerException {
        Scheduler scheduler = schedulerFactory.getScheduler();
        scheduler.setJobFactory(springBeanJobFactory);
        log.debug("Scheduler Enabled: {}", isSchedulerEnabled);
        if ("true".equals(isSchedulerEnabled)) {
            for (AbstractJob job : jobs) {
                JobDetail detail = JobBuilder.newJob()
                    .ofType(job.getClass())
                    .withIdentity(job.getName())
                    .build();

                Set<CronTrigger> triggers = new HashSet<>();
                for (JobConfigProperties.Trigger trigger : job.getTriggers()) {
                    triggers.add(TriggerBuilder.newTrigger()
                        .withIdentity(trigger.getTriggerName())
                        .withSchedule(CronScheduleBuilder.cronSchedule(trigger.getCronExpression())
                            .withMisfireHandlingInstructionDoNothing())
                        .build());
                }

                scheduler.scheduleJob(detail, triggers, true);
            }
        }
        return scheduler;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory(ApplicationContext applicationContext) {
        AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    @Primary
    public SchedulerFactoryBean factory(@Autowired @Qualifier("quartzDS") DataSource dataSource) {
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();
        factoryBean.setSchedulerName(SCHEDULER_NAME);

        factoryBean.setOverwriteExistingJobs(true);
        factoryBean.setAutoStartup(Boolean.parseBoolean(isSchedulerEnabled));
        if ("true".equals(isSchedulerEnabled)) {
            factoryBean.setDataSource(dataSource);
        }
        return factoryBean;
    }


    public static final class AutoWiringSpringBeanJobFactory extends SpringBeanJobFactory implements ApplicationContextAware {

        private AutowireCapableBeanFactory beanFactory;

        public void setApplicationContext(ApplicationContext applicationContext) {
            beanFactory = applicationContext.getAutowireCapableBeanFactory();
        }

        @Override
        protected Object createJobInstance(final TriggerFiredBundle bundle) throws Exception {
            final Object job = super.createJobInstance(bundle);
            beanFactory.autowireBean(job);
            return job;
        }
    }
}
