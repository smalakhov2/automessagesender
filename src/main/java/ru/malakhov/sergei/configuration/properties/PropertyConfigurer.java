package ru.malakhov.sergei.configuration.properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;
import ru.malakhov.sergei.exception.UnexpectedException;

@Configuration
@Validated
public class PropertyConfigurer {

    @Bean
    public JobConfigProperties.Job autoMessageSchedulerJobProperties(JobConfigProperties jobConfigProperties) {
        return jobConfigProperties.getJob("autoMessageScheduler")
            .orElseThrow(() -> new UnexpectedException("Can't find autoMessageScheduler config"));
    }

}
