package ru.malakhov.sergei.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "job-config")
public class JobConfigProperties {
    private List<Job> jobs;

    @Getter
    @Setter
    public static class Job {
        private String jobName;
        private List<Trigger> triggers;
    }

    @Getter
    @Setter
    public static class Trigger {
        private String triggerName;
        private String cronExpression;
        private Integer maxTries;
    }

    public Optional<Job> getJob(String jobName) {
        return jobs.stream().filter(job -> jobName.equals(job.jobName)).findFirst();
    }
}