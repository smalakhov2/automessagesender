package ru.malakhov.sergei.exception;

public class EmptyRecipientException extends AbstractException {

    public EmptyRecipientException() {
        super("Error! Recipient is empty...");
    }

}