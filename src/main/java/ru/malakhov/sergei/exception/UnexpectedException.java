package ru.malakhov.sergei.exception;

import org.jetbrains.annotations.NotNull;

public class UnexpectedException extends RuntimeException {
    public UnexpectedException(@NotNull String message) {
        super(message);
    }
}
