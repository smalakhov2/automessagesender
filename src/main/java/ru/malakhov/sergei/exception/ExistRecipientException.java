package ru.malakhov.sergei.exception;

public class ExistRecipientException extends AbstractException{


    public ExistRecipientException() {
        super("Error! Recipient is exist already...");
    }
}
