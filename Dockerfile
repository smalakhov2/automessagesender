FROM openjdk:11-jdk
MAINTAINER Sergei Malakhov <malahov20777@gmail.com>

COPY ./target/scheduler-for-tea.jar ./
EXPOSE 8080
ENTRYPOINT exec /usr/local/openjdk-8/bin/java -jar scheduler-for-tea.jar.jar