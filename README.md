# PROJECT INFO
AutoMessage Sender

The system implements automatic sending of messages to users from the database. A web interface has been implemented for easy interaction, there is a docker file for deployment.

# DEVELOPER INFO

**NAME:** Sergei Malakhov

**E-MAIL:**malahov20777@gmail.com

# SOFTWARE
- Maven
- JDK 11

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
